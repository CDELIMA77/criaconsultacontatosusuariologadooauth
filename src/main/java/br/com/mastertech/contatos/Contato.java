package br.com.mastertech.contatos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Contato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String donolista;
    private String contato;
    private String telefone;

    public Contato() {
    }

    public Contato(Integer id, String donolista, String contato, String telefone) {
        this.id = id;
        this.donolista = donolista;
        this.contato = contato;
        this.telefone = telefone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDonolista() {
        return donolista;
    }

    public void setDonolista(String donolista) {
        this.donolista = donolista;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
