package br.com.mastertech.contatos;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ContatoRepository extends CrudRepository<Contato , Integer>  {
    Iterable<Contato> findByDonolista(String donolista);
}
