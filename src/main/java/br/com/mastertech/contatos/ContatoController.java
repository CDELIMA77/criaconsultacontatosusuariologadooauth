package br.com.mastertech.contatos;

import br.com.mastertech.contatos.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @Autowired
    ContatoRepository contatoRepository;

    @PostMapping("/contato")
    public ResponseEntity<Contato> create(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario) {
        contato.setDonolista(usuario.getName());
        Contato contatoObjeto = contatoService.salvarContato(contato);
        return ResponseEntity.status(201).body(contatoObjeto);
    }

    @GetMapping("/contatos")
    public ResponseEntity<Iterable<Contato>> buscarContatos(@AuthenticationPrincipal Usuario usuario){
        String nome = usuario.getName();
        Iterable<Contato> contatoIterable = contatoRepository.findByDonolista(nome);
        return ResponseEntity.status(200).body(contatoIterable);
     }
}